您好，首先很高興收到您的履歷，在面試前我們有個考題，請您評估是否實作，請依照Figma上的資料執行以下幾點：

1. 請使用Vue3實作
2. Line Login登入實作，並將line的display name顯示在後台上
3. 串接發票數據資料API
4. 『所有年間的所有稅別』發票的數據資料匯出Excel
    * 欄位為年份、税別、數量、銷售總額
5. 目前已有電腦版，請依照figma刻手機版，包含登入頁面、系統內頁
6. 執行完畢後發MR到Master


### 前置步驟
1. 先git clone專案至本地
2. 至frontend_project資料夾、backend_project資料夾中安裝套件包
3. 至backend_project執行node index.js，backend將會啟動在本地上
4. 至frontend_project執行npm run dev，frontend將會啟動於本地上


### 使用框架、版本
1. node 14以上
2. Native-UI
3. Windicss
4. Pinia


### 參考資料
- [https://www.figma.com/file/fyBuwphnxER36RV9QswY1r/前端面試考題?node-id=0%3A1](https://www.figma.com/file/fyBuwphnxER36RV9QswY1r/%E5%89%8D%E7%AB%AF%E9%9D%A2%E8%A9%A6%E8%80%83%E9%A1%8C?node-id=0%3A1)
