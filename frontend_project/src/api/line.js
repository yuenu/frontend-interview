import axios from 'axios'
import qs from 'qs'

const CLINET_ID = 'YOUR_CLIENT_ID'
const CLIENT_SECRET_CODE = 'YOUR_CLIENT_SECRET_CODE'

const CALLBACK_URL = 'http://localhost:3000/callback'

export const GetLineAuthLink = async () => {
  // code here
  const url = 'https://access.line.me/oauth2/v2.1/authorize?'
  const params = new URLSearchParams('?')
  params.append('response_type', 'code')
  params.append('client_id', CLINET_ID)
  params.append('scope', 'profile')
  params.append('redirect_uri', CALLBACK_URL)
  params.append('state', '12345abcde')
  return url + params.toString()
}

export const GetLineAccessToken = async (code) => {
  // code here
  const rowBody = {
    grant_type: 'authorization_code',
    client_id: CLINET_ID,
    client_secret: CLIENT_SECRET_CODE,
    code: code,
    redirect_uri: 'http://localhost:3000/callback'
  }
  return await axios.post('https://api.line.me/oauth2/v2.1/token', qs.stringify(rowBody))
}

export const GetLineMeProfile = async (accessToken) => {
  // code here
  const headers = {
    Authorization: 'Bearer ' + accessToken,
  }
  return await axios.post('https://api.line.me/v2/profile', {}, {
    headers: headers,
  })
}
