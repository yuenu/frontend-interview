import axios from 'axios'

export const GetInoviceData = async ({ start, end }) => {
  // code here
  const response = await axios.get('http://localhost:3001/api/invoice', {
    params: {
      start,
      end,
    },
  })
  return response
}
